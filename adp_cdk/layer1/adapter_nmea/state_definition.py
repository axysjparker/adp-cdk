from aws_cdk import core

import aws_cdk.aws_stepfunctions as sfn
import aws_cdk.aws_lambda as lmb

class AdapterNMEA():
    def __init__(self, scope: core.Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

