from aws_cdk import core
from .adp_cdk_stack import AdpCdkStack

class DemoStage(core.Stage):
    def __init__(self, scope: core.Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

        l0 = AdpCdkStack(self, 'AdpCdk')