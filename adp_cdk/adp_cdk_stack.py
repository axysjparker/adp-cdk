from os import path
from aws_cdk import core

import aws_cdk.aws_lambda as lmb
import aws_cdk.aws_stepfunctions as sfn
import aws_cdk.aws_stepfunctions_tasks as tasks

import aws_cdk.aws_sns as sns

# from .layer1.adapter_nmea import state_definition.AdapterNMEA as nmea

class AdpCdkStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # The code that defines your stack goes here
        this_dir = path.dirname(__file__)

        lambda1 = lmb.Function(self, 'Layer0Lambda1',
            code=lmb.Code.from_asset(path.join(this_dir, 'layer0', 'lambda1')),
            runtime=lmb.Runtime.PYTHON_3_7,
            handler='handler.handler')

        sns_topic = sns.Topic(self, 'TestingSNS')
        lambda2 = lmb.Function(self, 'Layer0Lambda2',
            code=lmb.Code.from_asset(path.join(this_dir, 'layer0', 'lambda2')),
            runtime=lmb.Runtime.PYTHON_3_7,
            handler='handler.handler',
            environment={'SNSTOPIC':sns_topic.topic_arn})
        sns_topic.grant_publish(lambda2)

        t_l1 = tasks.LambdaInvoke(self, 'Lambda 1', 
            lambda_function=lambda1,
            output_path='$.Payload')

        t_l2 = tasks.LambdaInvoke(self, 'Lambda 2', 
            lambda_function=lambda2,
            output_path='$.Payload')

        definition = sfn.Chain.start(t_l1) \
            .next(t_l2)

        sfn.StateMachine(self, 'DemoStateMachine',
            definition = definition)