from aws_cdk import core

from aws_cdk import pipelines
from aws_cdk import aws_codepipeline as codepipeline
from aws_cdk import aws_codepipeline_actions as cpactions

from .demo_stage import DemoStage

class PipelineStack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

        source_artifact = codepipeline.Artifact()
        cloud_assembly_artifact = codepipeline.Artifact()

        pipeline = pipelines.CdkPipeline(self, 'DemoPipeline',
            cloud_assembly_artifact=cloud_assembly_artifact,
            pipeline_name='DemoPipeline',

            source_action=cpactions.BitBucketSourceAction(
                action_name='BitBucket-Jason',
                output=source_artifact,
                connection_arn='arn:aws:codestar-connections:us-west-2:053434365272:connection/cbc1a70e-b43f-444a-94ab-2fd3ff871cb4',
                owner='axysjparker',
                repo='adp-cdk'),
                
            synth_action=pipelines.SimpleSynthAction(
                source_artifact=source_artifact,
                cloud_assembly_artifact=cloud_assembly_artifact,
                install_command='npm install -g aws-cdk && pip install -r requirements.txt',
                synth_command='cdk synth')
        )

        pipeline.add_application_stage(DemoStage(self, 'Pre-prod', env={
            'account': '053434365272',
            'region': 'us-west-2'
        }))
