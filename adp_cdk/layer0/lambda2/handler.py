import boto3
import os

snstopic = os.environ.get('SNSTOPIC')

topic = boto3.resource('sns').Topic(snstopic)

def handler(event, context):
    print('writing to ' + snstopic)
    r = topic.publish(
        Message='HelloWorld!'
    )
    mid = r['MessageId']
    return {
        'body': f'Layer 0 Lambda 2 f{mid}',
        'statusCode': 200
    }