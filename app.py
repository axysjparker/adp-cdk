#!/usr/bin/env python3

from aws_cdk import core

from adp_cdk.deployment_pipeline_stack import PipelineStack

app = core.App()
PipelineStack(app, 'PipelineStack', env = {
    'account': '053434365272',
    'region': 'us-west-2'
})

app.synth()
